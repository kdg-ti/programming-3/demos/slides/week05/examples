package be.kdg.pro3.m05.s01.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S01RequestparamHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(S01RequestparamHelloApplication.class, args);
	}

}
