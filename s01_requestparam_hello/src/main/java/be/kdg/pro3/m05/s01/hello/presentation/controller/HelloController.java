package be.kdg.pro3.m05.s01.hello.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloController {



	@GetMapping("/hello")
	public String sayHello(@RequestParam("name") String guest, Model model) {
		model.addAttribute("helloname", "Hello " + guest);
		return "hello";
	}

		// if the request parameter matches method parameter name, it's value is assigned to the method parameter
	@GetMapping("/")
	public String defaultHello( String name, Model model) {
		model.addAttribute("helloname", "Hello " + name);
		return "hello";
	}

	@GetMapping("/hello/{name}")
	public String sayHelloWithPath(@PathVariable("name") String name, Model model) {
		model.addAttribute("helloname", "Hello " + name);
		return "hello";
	}


}
