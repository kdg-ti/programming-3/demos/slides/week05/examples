package be.kdg.pro3.s02_sms_form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S02SmsFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(S02SmsFormApplication.class, args);
	}

}
