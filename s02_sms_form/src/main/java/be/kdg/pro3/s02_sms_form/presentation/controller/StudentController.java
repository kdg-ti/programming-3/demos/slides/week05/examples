package be.kdg.pro3.s02_sms_form.presentation.controller;


import be.kdg.pro3.s02_sms_form.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
public class StudentController {
    private static Logger logger = LoggerFactory.getLogger(StudentController.class);
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }




    @PostMapping("/students/add")
    public String processAddStudent(@RequestParam("fname") String firstname, String lastname, @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate birthdate, Double length, Integer credits) {
        logger.info("Processing " + firstname + " " + birthdate + " " + length + " " + credits);
        studentService.addStudent(firstname,
          lastname,
          birthdate,
         length,
          credits);
        return "addstudent";
    }

    @GetMapping("/students/add")
    public String addStudent() {
        return "addstudent";
    }

}
