package be.kdg.pro3.s02_sms_form.service;

import be.kdg.pro3.s02_sms_form.domain.Student;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class StudentService { //TODO: Should be interface and implementation!
    public void addStudent(String firstname, String lastname, LocalDate birthday, Double length, Integer credits) {
        //TODO: maybe some business logic and insert into the repository...
        Student s = new Student(firstname + " " + lastname, birthday, length, credits);//TODO...
        //TODO: add to repository...
    }
}
