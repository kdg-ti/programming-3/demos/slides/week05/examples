package be.kdg.pro3.s03_sms_viewmodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S03SmsViewmodelApplication {

	public static void main(String[] args) {
		SpringApplication.run(S03SmsViewmodelApplication.class, args);
	}

}
