package be.kdg.pro3.s03_sms_viewmodel.domain;

import java.time.LocalDate;

public class Student {
    private String name;//this field does not map to the viewmodel fields!
    private LocalDate birthday;
    private double length;
    private int credits;
    private StudentType studentType;

    public Student(String name, LocalDate birthday, double length, int credits) {
        this.name = name;
        this.birthday = birthday;
        this.length = length;
        this.credits = credits;
    }

    public Student(String name,
      LocalDate birthday,
      double length,
      int credits,
      StudentType studentType) {
        this(name ,birthday,length,credits);
        this.studentType = studentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public StudentType getStudentType() {
        return studentType;
    }

    public void setStudentType(StudentType studentType) {
        this.studentType = studentType;
    }

    @Override
    public String toString() {
        return "Student{" +
          "name='" + name + '\'' +
          ", birthday=" + birthday +
          ", length=" + length +
          ", credits=" + credits +
          ", studentType=" + studentType +
          '}';
    }
}
