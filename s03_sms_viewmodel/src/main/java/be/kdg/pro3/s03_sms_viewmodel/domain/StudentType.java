package be.kdg.pro3.s03_sms_viewmodel.domain;

public enum StudentType {
	ACS, REGULAR, FLEX
}
