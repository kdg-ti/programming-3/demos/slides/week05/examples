package be.kdg.pro3.s03_sms_viewmodel.presentation.configuration;

import be.kdg.pro3.s03_sms_viewmodel.presentation.converter.StringToStudentTypeConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToStudentTypeConverter());
    }

}
