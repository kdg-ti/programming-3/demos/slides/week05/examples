package be.kdg.pro3.s03_sms_viewmodel.presentation.controller;


import be.kdg.pro3.s03_sms_viewmodel.presentation.viewmodel.StudentViewModel;
import be.kdg.pro3.s03_sms_viewmodel.service.StudentService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class StudentController {
	private static Logger logger = LoggerFactory.getLogger(StudentController.class);
	private StudentService studentService;

	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}


	@PostMapping("/students/add")
	public String processAddStudent(@Valid @ModelAttribute("student") StudentViewModel viewModel,
		BindingResult result) {
		logger.info("Processing " + viewModel.toString());
		if (result.hasErrors()) {
			result.getAllErrors().forEach(e -> logger.warn(e.toString()));
		} else {
			logger.info("No validation errors, adding the student ");
			studentService.addStudent(String.format("%s %s",
					viewModel.getFirstname(),
					viewModel.getLastname()),
				viewModel.getBirthdate(),
				viewModel.getLength(),
				viewModel.getCredits(),
				viewModel.getStudentType());
		}
		return "addstudent";
	}

	@GetMapping("/students/add")
	public String getAddStudentForm(Model model) {
		//add an empty viewmodel to the form, to be able to use it in thyemleaf to
		//generate the name= and id= attributes....
		model.addAttribute("student", new StudentViewModel());
		return "addstudent";
	}

}
