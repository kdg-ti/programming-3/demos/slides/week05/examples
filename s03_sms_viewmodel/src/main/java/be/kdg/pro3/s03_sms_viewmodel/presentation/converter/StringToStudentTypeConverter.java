package be.kdg.pro3.s03_sms_viewmodel.presentation.converter;

import be.kdg.pro3.s03_sms_viewmodel.domain.StudentType;
import org.springframework.core.convert.converter.Converter;
import static be.kdg.pro3.s03_sms_viewmodel.domain.StudentType.*;

public class StringToStudentTypeConverter implements Converter<String, StudentType> {
    @Override
    public StudentType convert(String source) {
        return switch (source.substring(0,Math.min(source.length(),3)).toUpperCase()){
            case "ACS" -> ACS;
            case "FLE" -> FLEX;
            default    -> REGULAR;
        };
    }
}
