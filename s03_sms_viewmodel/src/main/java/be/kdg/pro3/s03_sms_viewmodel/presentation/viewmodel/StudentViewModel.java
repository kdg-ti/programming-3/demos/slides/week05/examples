package be.kdg.pro3.s03_sms_viewmodel.presentation.viewmodel;

import be.kdg.pro3.s03_sms_viewmodel.domain.StudentType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Positive;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class StudentViewModel {
    @NotBlank(message = "firstname should not be empty!")
    private String firstname;

    @NotBlank(message = "lastname should not be empty!")
    private String lastname;

    @Past(message = "your birthday should be in the past...")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    @Positive(message = "Length should be positive")
    private Double length;
    private Integer credits;
    private StudentType studentType;

    public StudentViewModel() {
    }

    public StudentViewModel(String firstname, String lastname, LocalDate birthdate, Double length, Integer credits) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.length = length;
        this.credits = credits;
    }

    public StudentViewModel(String firstname, String lastname,
      LocalDate birthday,
      double length,
      int credits,
      StudentType studentType) {
        this(firstname,lastname ,birthday,length,credits);
        this.studentType = studentType;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public Double getLength() {
        return length;
    }

    public Integer getCredits() {
        return credits;
    }

    public StudentType getStudentType() {
        return studentType;
    }

    public void setStudentType(StudentType studentType) {
        this.studentType = studentType;
    }

    @Override
    public String toString() {
        return "StudentViewModel{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                ", length=" + length +
                ", credits=" + credits +
                ", type=" + studentType +
                '}';
    }
}
