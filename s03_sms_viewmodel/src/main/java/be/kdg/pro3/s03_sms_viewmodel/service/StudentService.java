package be.kdg.pro3.s03_sms_viewmodel.service;

import be.kdg.pro3.s03_sms_viewmodel.domain.Student;
import be.kdg.pro3.s03_sms_viewmodel.domain.StudentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class StudentService { //TODO: Should be interface and implementation!
    private static Logger log = LoggerFactory.getLogger(StudentService.class);

    public void addStudent(String name, LocalDate birthday, Double length, Integer credits,
      StudentType type) {
        //TODO: maybe some business logic
        Student s = new Student(name, birthday, length, credits);
                log.info("Adding Student " + s);

        //TODO: add to repository...
    }
}
